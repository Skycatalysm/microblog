# CakePHP 2 Microblog
running on cakephp 3

## Installating

Run bash then cd to the root directory
```
cd cakephp3
```

Run composer update
```
composer update
```

Cd inside config folder(/microblog/config)
```
cd config
```

Create a database then import the microblog.sql inside the config

Copy, paste and rename database.php.default to database.php by running
```
cp database.php.default database.php
```

Open the new database.php file via a text editor, find then change the config for the database.

Open email.php then put your email on the $gmail configuration

run the app on a virtual hosts, because there is still some bugs regarding the routings