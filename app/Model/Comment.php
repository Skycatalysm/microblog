<?php

class Comment extends AppModel
{
	public $actsAs = array('Containable');
	public $belongsTo = array(
		'User',
		'Post'
	);
	public $validate = array(
		'content' => array(
			'required' => array(
				'rule' => 'notBlank',
				'message' => 'Please say something...'
			),
			'limitLength' => array(
				'rule' => array('maxLength', 140),
				'message' => 'Comment must be not larger than 140 characters long...'
			)
		)
	);

	public function isOwnedBy($post, $user)
	{
		return $this->field('id', array('id' => $post, 'user_id' => $user)) !== false;
	}
}
