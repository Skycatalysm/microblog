<?php

class Post extends AppModel
{
	public $actsAs = array('Containable');
	public $belongsTo = array(
		'User',
		'SharedPost' => array(
			'className' => 'Post',
			'foreignKey' => 'shared_post_id'
		),
	);
	public $hasMany = array(
		'Like' => array(
			'conditions' => array('status' => 1)
		),
		'Comment' => array(
			'conditions' => array('status' => 1)
		)
	);
	public $validate = array(
		'content' => array(
			'required' => array(
				'rule' => 'notBlank',
				'message' => 'Please say something...'
			),
			'limitLength' => array(
				'rule' => array('maxLength', 140),
				'message' => 'Post must be not larger than 140 characters long...'
			)
		)
	);

	public function isOwnedBy($post, $user)
	{
		return $this->field('id', array('id' => $post, 'user_id' => $user)) !== false;
	}
}
