<?php

class Like extends AppModel
{
	public $actsAs = array('Containable');
	public $belongsTo = array(
		'User',
		'Post'
	);
	public $validate = array();
}
