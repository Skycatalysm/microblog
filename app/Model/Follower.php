<?php

class Follower extends AppModel
{
	public $actsAs = array('Containable');
	public $belongsTo = array(
		'User',
		'FollowedUser' => array(
			'className' => 'User',
			'foreignKey' => 'followed_user_id'
		)
	);
	public $validate = array();
}
