<?php

class ProfilePicture extends AppModel
{
	public $actsAs = array('Containable');
	public $belongsTo = array('User');

	public $validate = array(
		'username' => array(
			'required' => array(
				'rule' => 'notBlank',
				'message' => 'A username is required.'
			)
		),
	);

	public function isOwnedBy($profilePicture, $user)
	{
		return $this->field('id', array('id' => $profilePicture, 'user_id' => $user)) !== false;
	}

	/**
	 * Returns a default value if not existing on the database
	 * @param $userId
	 * @return array|int|null
	 */
	public function showProfilePicture($userId)
	{
		$profilePicture = $this->find('first', array('conditions' => array('user_id' => $userId),
			'order' => array('id' => 'DESC')));
		if (empty($profilePicture)) {
			$profilePicture['ProfilePicture']['image_name'] = 'default_profile_picture.png';
			return $profilePicture;
		}
		return $profilePicture;
	}
}
