<?php

class User extends AppModel
{
	public $actsAs = array('Containable');
	public $hasMany = array(
		'Comment',
		'Post',
		'ProfilePicture' => array(
			'order' => 'ProfilePicture.id DESC',
			'limit' => 1,
		),
		'Followings' => array(
			'className' => 'Followers',
			'foreignKey' => 'user_id',
			'conditions' => array('status' => 1)
		),
		'Followers' => array(
			'className' => 'Followers',
			'foreignKey' => 'followed_user_id',
			'conditions' => array('status' => 1)
		)
	);


	public $validate = array(
		'username' => array(
			'required' => array(
				'rule' => 'notBlank',
				'message' => 'A username is required.'
			),
			'unique' => array(
				'rule' => 'isUnique',
				'message' => 'This username has already been taken.'
			),
			'limitLength' => array(
				'rule' => array('maxLength', 140),
				'message' => 'Username must be not larger than 140 characters long...'
			)
		),
		'password' => array(
			'required' => array(
				'rule' => 'notBlank',
				'message' => 'A password is required.'
			),
			'passwordConfirm' => array(
				'rule' => array('passwordConfirm'),
				'message' => 'Password confirmation must match password.',
			),
			'minLength' => array(
				'rule' => array('minLength', '8'),
				'message' => 'Password should be at least 8 characters long.'
			),
			'limitLength' => array(
				'rule' => array('maxLength', 140),
				'message' => 'Password must be not larger than 140 characters long...'
			)
		),
		'email' => array(
			'required' => array(
				'rule' => 'notBlank',
				'message' => 'An email is required.'
			),
			'email' => array(
				'rule' => array('email', true),
				'message' => 'Please supply a valid email address.'
			),
			'unique' => array(
				'rule' => 'isUnique',
				'message' => 'This email has already been taken.'
			),
			'limitLength' => array(
				'rule' => array('maxLength', 140),
				'message' => 'Email must be not larger than 140 characters long...'
			)
		),
		'first_name' => array(
			'required' => array(
				'rule' => 'notBlank',
				'message' => 'A first name is required.'
			),
			'onlyLetters' => array(
				'rule' => array('onlyLetters'),
				'message' => 'Please supply a valid first name.'
			),
			'limitLength' => array(
				'rule' => array('maxLength', 140),
				'message' => 'First name must be not larger than 140 characters long...'
			)
		),
		'last_name' => array(
			'required' => array(
				'rule' => 'notBlank',
				'message' => 'A last name is required.'
			),
			'onlyLetters' => array(
				'rule' => array('onlyLetters'),
				'message' => 'Please supply a valid last name.'
			),
			'limitLength' => array(
				'rule' => array('maxLength', 140),
				'message' => 'Last name must be not larger than 140 characters long...'
			)
		),
	);

	public function onlyLetters($check)
	{
		return ctype_alpha(end($check));
	}

	public function passwordConfirm()
	{
		if ($this->data[$this->alias]['password'] !== $this->data[$this->alias]['confirmPassword']) {
			return false;
		}
		return true;
	}


	public function beforeSave($options = array())
	{
		if (isset($this->data[$this->alias]['password'])) {
			$passwordHasher = new BlowfishPasswordHasher();
			$this->data[$this->alias]['password'] = $passwordHasher->hash(
				$this->data[$this->alias]['password']
			);
		}
		return true;
	}

	public function isOwnedBy($profileId, $user)
	{
		return $this->field('id', array('id' => $profileId, 'id' => $user)) !== false;
	}
}
