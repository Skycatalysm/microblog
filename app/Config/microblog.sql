-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 07, 2019 at 07:58 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `microblog`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `content` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `post_id`, `content`, `created`, `updated`, `status`) VALUES
(7, 48, 50, 'hey', '2019-10-04 10:36:46', '2019-10-04 10:36:46', 1),
(8, 48, 64, 'test', '2019-10-07 04:24:39', '2019-10-07 04:30:17', 0),
(9, 48, 42, 'hahaha', '2019-10-07 04:32:30', '2019-10-07 04:32:30', 1),
(10, 48, 45, 'testing nga', '2019-10-07 04:42:53', '2019-10-07 04:42:53', 1);

-- --------------------------------------------------------

--
-- Table structure for table `followers`
--

CREATE TABLE `followers` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `followed_user_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `followers`
--

INSERT INTO `followers` (`id`, `user_id`, `followed_user_id`, `status`, `created`, `updated`) VALUES
(150, 51, 48, 0, '2019-10-04 10:20:36', '2019-10-04 10:46:51'),
(151, 49, 51, 0, '2019-10-04 10:20:37', '2019-10-04 10:46:52'),
(152, 52, 48, 0, '2019-10-04 10:20:38', '2019-10-04 10:46:52'),
(153, 48, 53, 0, '2019-10-04 10:20:38', '2019-10-07 05:02:32'),
(154, 50, 48, 0, '2019-10-04 10:20:39', '2019-10-04 10:46:53'),
(155, 48, 54, 0, '2019-10-04 10:21:01', '2019-10-07 03:15:51'),
(156, 48, 60, 0, '2019-10-04 10:21:14', '2019-10-07 05:02:21'),
(157, 48, 57, 0, '2019-10-04 10:46:39', '2019-10-07 03:15:49'),
(158, 48, 58, 1, '2019-10-04 10:46:40', '2019-10-04 10:46:40'),
(159, 48, 55, 1, '2019-10-04 10:46:41', '2019-10-04 10:46:41'),
(160, 48, 56, 1, '2019-10-04 10:46:41', '2019-10-04 10:46:41'),
(161, 48, 61, 0, '2019-10-04 10:46:44', '2019-10-07 05:02:21'),
(162, 48, 62, 0, '2019-10-04 10:46:45', '2019-10-07 05:02:22'),
(163, 48, 59, 1, '2019-10-04 10:46:47', '2019-10-04 10:46:47'),
(164, 48, 51, 0, '2019-10-04 11:22:18', '2019-10-07 05:02:29'),
(165, 48, 49, 0, '2019-10-07 03:13:14', '2019-10-07 05:02:30'),
(166, 48, 52, 0, '2019-10-07 03:13:15', '2019-10-07 05:02:30'),
(167, 48, 50, 0, '2019-10-07 03:13:16', '2019-10-07 05:02:31');

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `post_id`, `user_id`, `created`, `updated`, `status`) VALUES
(13, 50, 48, '2019-10-07 04:24:13', '2019-10-07 04:24:13', 1),
(14, 64, 48, '2019-10-07 04:28:02', '2019-10-07 04:28:42', 1),
(15, 42, 48, '2019-10-07 04:33:50', '2019-10-07 05:00:17', 1),
(16, 45, 48, '2019-10-07 04:43:00', '2019-10-07 04:43:00', 1),
(17, 49, 48, '2019-10-07 05:00:20', '2019-10-07 05:00:20', 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `content` longtext NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `shared_post_id` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `content`, `image_name`, `shared_post_id`, `created`, `status`) VALUES
(40, 50, 'forty', '', NULL, '2019-10-02 10:01:33', 1),
(41, 51, 'Good evening :)', '', NULL, '2019-10-02 10:05:43', 1),
(42, 52, 'Good evening :)', '', NULL, '2019-10-02 10:06:39', 1),
(43, 48, 'hi', '', NULL, '2019-10-02 11:25:46', 1),
(44, 48, 'test', '', NULL, '2019-10-03 03:09:32', 1),
(45, 56, 'Good morning everyone\r\n\r\nIm happy for you', '', NULL, '2019-10-03 03:10:44', 1),
(46, 48, 'test\r\ntest tea teat\r\nasds', '', NULL, '2019-10-03 04:16:44', 1),
(48, 48, 'test', '', 43, '2019-10-03 07:33:06', 1),
(49, 48, 'test', '', 43, '2019-10-03 07:33:18', 1),
(50, 48, 'shared post', '', 49, '2019-10-03 07:34:17', 1),
(64, 49, 'testing', '', NULL, '2019-10-04 05:05:12', 1),
(66, 48, 'goodmorning din', '', 45, '2019-10-07 04:31:20', 1),
(67, 48, 'test', '', NULL, '2019-10-07 05:54:29', 0),
(68, 48, 'test', '', NULL, '2019-10-07 05:54:44', 0),
(69, 48, 'test', '', NULL, '2019-10-07 05:55:35', 0),
(73, 48, 'shared', '5d9ac5bbad879pipizza.jpg', NULL, '2019-10-07 06:57:31', 1),
(74, 48, 'shared pizza\r\n', '', 73, '2019-10-07 07:04:28', 1);

-- --------------------------------------------------------

--
-- Table structure for table `profile_pictures`
--

CREATE TABLE `profile_pictures` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_pictures`
--

INSERT INTO `profile_pictures` (`id`, `user_id`, `image_name`, `created`) VALUES
(49, 56, '5d9314224f7a2slime.jpg', '2019-10-01 10:53:54'),
(50, 58, '5d9314224f7a2slime.jpg', '0000-00-00 00:00:00'),
(51, 48, '5d9461579b79bbeac96b8e13d2198fd4bb1d5ef56cdcf.jpg', '2019-10-02 10:35:35');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `email_hash` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `first_name`, `last_name`, `created`, `updated`, `status`, `email_hash`) VALUES
(48, 'sky', 'skycatalysm@gmail.com', '$2a$10$cbdJ0m8KCydvPIO0HPvD9.dHd0ArnuirTSCeIHCFNG45BslMiDf1.', 'Sky', 'Catalysm', '2019-09-30 10:03:16', '2019-10-07 05:34:21', 1, 'fcd797b8a481a0e45b569ea01ccbfb83b721f312'),
(49, 'test2', 'test2@gmail.com', '$2a$10$QXMdep/QU2JUNLThqpOUiO.wy1wvNzCPiWd40KyQTkrSP1QyV2RH.', 'Brian', 'Brian', '2019-09-30 10:38:57', '2019-09-30 10:38:57', 0, '7e699e3c7ccb72968dd36ad6dcbcb04cef2877dd'),
(50, 'fix1', 'fix1@gmail.com', 'asdasdad', 'Brian', 'Brian', '2019-09-18 00:00:00', '2019-09-19 00:00:00', 0, 'adasdasdsad'),
(51, 'fix2', 'fix2@gmail.com', 'asdasdad', 'Brian', 'Brian', '2019-09-18 00:00:00', '2019-09-19 00:00:00', 0, 'adasdasdsad'),
(52, 'fix4', 'fix3@gmail.com', 'asdasdad', 'Brian', 'Brian', '2019-09-18 00:00:00', '2019-09-19 00:00:00', 0, 'adasdasdsad'),
(53, 'fix4', 'fix4@gmail.com', 'asdasdad', 'Brian', 'Brian', '2019-09-18 00:00:00', '2019-09-19 00:00:00', 0, 'adasdasdsad'),
(54, 'fix5', 'fix5@gmail.com', 'asdasdad', 'Brian', 'Brian', '2019-09-18 00:00:00', '2019-09-19 00:00:00', 0, 'adasdasdsad'),
(55, 'fix6', 'fix6@gmail.com', 'asdasdad', 'Brian', 'Brian', '2019-09-18 00:00:00', '2019-09-19 00:00:00', 0, 'adasdasdsad'),
(56, 'fix7', 'fix7@gmail.com', 'asdasdad', 'Brian', 'Brian', '2019-09-18 00:00:00', '2019-09-19 00:00:00', 0, 'adasdasdsad'),
(57, 'fix21', 'fix21@gmail.com', 'asdasdad', 'Brian', 'Brian', '2019-09-18 00:00:00', '2019-09-19 00:00:00', 0, 'adasdasdsad'),
(58, 'fix22', 'fix22@gmail.com', 'asdasdad', 'Brian', 'Brian', '2019-09-18 00:00:00', '2019-09-19 00:00:00', 0, 'adasdasdsad'),
(59, 'fix23', 'fix23@gmail.com', 'asdasdad', 'Brian', 'Brian', '2019-09-18 00:00:00', '2019-09-19 00:00:00', 0, 'adasdasdsad'),
(60, 'fix24', 'fix24@gmail.com', 'asdasdad', 'Brian', 'Brian', '2019-09-18 00:00:00', '2019-09-19 00:00:00', 0, 'adasdasdsad'),
(61, 'fix25', 'fix25@gmail.com', 'asdasdad', 'Brian', 'Brian', '2019-09-18 00:00:00', '2019-09-19 00:00:00', 0, 'adasdasdsad'),
(62, 'fix26', 'fix26@gmail.com', 'asdasdad', 'Brian', 'Brian', '2019-09-18 00:00:00', '2019-09-19 00:00:00', 0, 'adasdasdsad'),
(64, 'skynew', 'skycatalysssm@gmail.com', '$2a$10$2aGco6cmA8IRjQIbCsUmJ./iYMKmswbL69hRb5gAlIoKfe8jUpUs2', 'new', 'me', '2019-10-07 07:53:47', '2019-10-07 07:54:21', 1, '135c76348480a3eaf5aa7a2b3a88094dccc94032');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `followers`
--
ALTER TABLE `followers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `profile_pictures`
--
ALTER TABLE `profile_pictures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `followers`
--
ALTER TABLE `followers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=168;

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `profile_pictures`
--
ALTER TABLE `profile_pictures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
