$(document).ready(function () {
	//For automatically closing flash messages
	$(".alert").delay(4000).slideUp(200, function () {
		$(this).alert('close');
	});

	//For delete post modal
	$('.delete-post-modal-trigger').click(function () {
		let deleteBody = $(this).attr('href');
		let modalToToggle = $(this).attr('data-target');
		let currentPost = $(this).attr('data-post-id');
		let currentPostContent = $(currentPost).html();
		$("#deletePostModalBody").html(currentPostContent);
		$("#deletePostUrl").attr('href', deleteBody)
		$(modalToToggle).modal();
	});

	//For delete comment modal
	$(document).delegate('.delete-comment-modal-trigger', 'click', function () {
		let deleteBody = $(this).attr('href');
		let modalToToggle = $(this).attr('data-target');
		let currentComment = $(this).attr('data-comment-id');
		let currentCommentContent = $(currentComment).html();
		$("#deleteCommentModalBody").html(currentCommentContent);
		$("#deleteCommentUrl").attr('href', deleteBody)
		$(modalToToggle).modal();
	});

	//For edit post modal
	$('.edit-post-modal-trigger').click(function () {
		let modalToToggle = $(this).attr('data-target');
		let currentPost = $(this).attr('data-post-id');
		let sharedPost = $('#post-rendered' + currentPost);
		let currentPostContent = $('#post-content' + currentPost).text();
		let sharedPostContent = $(sharedPost).html();
		let currentPostImage = $('#post-image' + currentPost).html();
		$("#data-post-content-target").val(currentPostContent.trim());
		$("#data-post-shared-content-target").html(currentPostImage + sharedPostContent);
		$("#data-post-id-target").val(currentPost);
		$(modalToToggle).modal();
	});


	//For share post modal
	$('.share-post-modal-trigger').click(function () {
		let modalToToggle = $(this).attr('data-target');
		let currentPostId = $(this).attr('data-post-id');
		let currentSharedPostContent = $('#sharing-post-content' + currentPostId).html();
		$("#share-post-content-target").html(currentSharedPostContent);
		$("#shareIdTarget").val(currentPostId);
		$(modalToToggle).modal();
	});

	//For ajax like-unlike
	$(".ajaxLike").click(function () {
		let dataLikedId = $(this).attr('data-liked-id');
		$(this).attr('disabled', true);
		$.post("/likes/react",
			{
				postId: dataLikedId
			},
			function (data) {
				let arr = $.parseJSON(data);
				let likes = '';
				if (arr['postLikeCount'] === 1) {
					likes = arr['postLikeCount'] + ' like.'
				} else if (arr['postLikeCount'] > 1) {
					likes = arr['postLikeCount'] + ' likes.'
				}
				let likeStatus = 'like';
				if (arr['likeStatus'] === 1) {
					likeStatus = 'unlike';
				} else if (arr['likeStatus'] === 0) {
					likeStatus = 'like';
				}
				if (!(typeof arr['notif'] === "undefined")) {
					alert(arr['notif']);
					return;
				}
				$('#likeCount' + dataLikedId).html(likes);
				$("[data-liked-id=" + dataLikedId + "]").html(likeStatus);
			}
		);
		$(this).attr('disabled', false);
	});

	//ajax comments request
	$(".ajax-comments").click(function () {
		let $loadCommentsButton = $(this);
		let $postId = ($loadCommentsButton.attr('data-show-comments-target'));
		let $postPageValue = ($loadCommentsButton.attr('data-show-comments-value'));
		$.get('/comments/ajaxComments/' + $postId + '/page:' + $postPageValue, function (data, status) {
			let arr = $.parseJSON(data);
			if (arr === 'failed') {
				$loadCommentsButton.attr('class', 'text-secondary');
				$loadCommentsButton.html('Nothing more to show...');
				return;
			}
			for (let i = 0; i < arr.length; i++) {
				//clone the div
				let $bluePrint = $('#comment-blueprint').clone();
				// comment content
				let commentId = arr[i]['Comment']['id'];
				let commentContent = arr[i]['Comment']['content'];
				let commentCreated = new Date(arr[i]['Comment']['created']);

				//user picture
				let commentUserPicture = 'default_profile_picture.png';
				try {
					commentUserPicture = arr[i]['User']['ProfilePicture'][0]['image_name'];
				} catch (e) {
					commentUserPicture = 'default_profile_picture.png';
				}
				let userImagePath = $(".comment-user-picture", $bluePrint).attr('src');
				let userImagePathCount = userImagePath.split('/');
				userImagePathCount[userImagePathCount.length - 1] = commentUserPicture; // new value
				commentUserPicture = userImagePathCount.join('/');

				//user info
				//full name
				let commentUserFullName = arr[i]['User']['first_name'] + ' ' + arr[i]['User']['last_name'];
				//user id for url
				let commentUserId = arr[i]['User']['id'];
				let commentUserIdPath = $(".comment-user-full-name", $bluePrint).attr('href');
				let userIdPathCountCount = commentUserIdPath.split('/');
				userIdPathCountCount[userIdPathCountCount.length - 1] = commentUserId; // new value
				commentUserIdPath = userIdPathCountCount.join('/');
				//user email
				let commentUserEmail = arr[i]['User']['email'];


				//dom for deleting
				let createDivDom = '';
				if ($bluePrint.attr('data-comment-auth') === commentUserId) {
					createDivDom = '<a href="/comments/delete/' + commentId + '" class="close text-secondary delete-comment-modal-trigger" onclick="event.preventDefault();" data-target="#deleteCommentModal" data-comment-id="#comment-id' + commentId + '">×</a>'
				}


				// comment content to dom
				$(".comment-content", $bluePrint).text(commentContent);
				$(".comment-created", $bluePrint).text('(' + formatDate(commentCreated) + ')');
				$bluePrint.attr('id', 'comment-id' + commentId);

				//user picture to dom
				$(".comment-user-picture", $bluePrint).attr('src', commentUserPicture);

				//user info to dom
				$(".comment-user-full-name", $bluePrint).text(commentUserFullName);
				$(".comment-user-full-name", $bluePrint).attr('href', commentUserIdPath);
				$(".comment-user-email", $bluePrint).text(commentUserEmail);

				let clonedComment = $('#clone-comment' + $postId);
				clonedComment.append('<hr />');
				clonedComment.append(createDivDom);
				clonedComment.append($bluePrint);
			}
			$postPageValue++;
			$loadCommentsButton.attr('data-show-comments-value', $postPageValue)
		});
	});

});

function formatDate(date) {
	const dayNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
	const monthNames = ["January", "February", "March", "April", "May", "June",
		"July", "August", "September", "October", "November", "December"
	];
	let hours = date.getHours();
	let minutes = date.getMinutes();
	let ampm = hours >= 12 ? 'pm' : 'am';
	hours = hours % 12;
	hours = hours ? hours : 12; // the hour '0' should be '12'
	minutes = minutes < 10 ? '0' + minutes : minutes;
	let strTime = hours + ':' + minutes + ' ' + ampm;
	return dayNames[date.getDay()] + ', ' + date.getDate() + " " + monthNames[date.getMonth()] + " " + date.getFullYear() + "~ " + strTime;
}
