<?php

class FollowersController extends AppController
{
	/**
	 * Add to components
	 * @var array
	 */
	public $components = array('Paginator');

	/**
	 *Check authorization
	 * @param $user
	 * @return bool
	 */
	public function isAuthorized($user)
	{
		if ($this->action === 'follow' || $this->action === 'unfollow' || $this->action === 'followings'
			|| $this->action === 'followers') {
			return true;
		}
	}

	/**
	 * Follow a profile
	 * @return CakeResponse|null
	 * @throws Exception
	 */
	public function follow()
	{
		if ($this->request->is('post')) {

			$followedUserId = $this->request->data['follower']['followed_user_id'];

			// Check if the user is trying to follow himself
			if ((int)$this->Auth->user('id') === (int)$followedUserId) {
				$this->Flash->error(__('You cant follow yourself.'));
				return $this->redirect(Router::url($this->referer(), true));
			}

			$conditions = array(
				'user_id' => $this->Auth->user('id'),
				'followed_user_id' => (int)$followedUserId
			);

			if ($this->Follower->hasAny($conditions)) {
				$follower = $this->Follower->find('first', array('conditions' => $conditions));
				$this->Follower->read('status', $follower['Follower']['id']);
				$this->Follower->set(array(
					'status' => 1
				));
				if ($this->Follower->save()) {
					$this->Flash->success(__('Followed.'));
					return $this->redirect(Router::url($this->referer(), true));
				}
			}

			$this->Follower->create();
			$this->Follower->set(array(
				'user_id' => $this->Auth->user('id'),
				'followed_user_id' => (int)$followedUserId,
				'status' => 1
			));
			if ($this->Follower->save()) {
				$this->Flash->success(__('Followed.'));
				return $this->redirect(Router::url($this->referer(), true));
			}
			foreach ($this->Follower->validationErrors as $validationErrors) {
				foreach ($validationErrors as $validationError) {
					$this->Flash->error(__($validationError));
				}
			}
			return $this->redirect(Router::url($this->referer(), true));
		}
	}

	/**
	 * To unfollow a profile
	 * @return CakeResponse|null
	 * @throws Exception
	 */
	public function unfollow()
	{

		$followedUserId = $this->request->data['follower']['followed_user_id'];

		// Check if the user is trying to follow himself
		if ((int)$this->Auth->user('id') === (int)$followedUserId) {
			$this->Flash->error(__('You cant follow yourself.'));
			return $this->redirect(Router::url($this->referer(), true));
		}

		$conditions = array(
			'user_id' => $this->Auth->user('id'),
			'followed_user_id' => (int)$followedUserId
		);

		if ($this->Follower->hasAny($conditions)) {
			$follower = $this->Follower->find('first', array('conditions' => $conditions));
			$this->Follower->read('status', $follower['Follower']['id']);
			$this->Follower->set(array(
				'status' => 0
			));
			if ($this->Follower->save()) {
				$this->Flash->success(__('Unfollowed.'));
				return $this->redirect(Router::url($this->referer(), true));
			}
		}

		$this->Follower->create();
		$this->Follower->set(array(
			'user_id' => $this->Auth->user('id'),
			'followed_user_id' => (int)$followedUserId,
			'status' => 0
		));
		if ($this->Follower->save()) {
			$this->Flash->success(__('Unfollowed.'));
			return $this->redirect(Router::url($this->referer(), true));
		}
		foreach ($this->Follower->validationErrors as $validationErrors) {
			foreach ($validationErrors as $validationError) {
				$this->Flash->error(__($validationError));
			}
		}
		return $this->redirect(Router::url($this->referer(), true));
	}

	/**
	 * View for the list of followings
	 * @param $id
	 */
	public function followings($id)
	{
		$this->Paginator->settings = array(
			'limit' => 6,
			'conditions' => array(
				'Follower.user_id' => $id,
				'Follower.status' => 1
			),
			'contain' => array(
				'User' => array('ProfilePicture' => array(
					'order' => 'ProfilePicture.id DESC',
					'limit' => 1
				)),
				'FollowedUser' => array('ProfilePicture' => array(
					'order' => 'ProfilePicture.id DESC',
					'limit' => 1
				))
			)
		);
		$data = $this->Paginator->paginate('Follower');
		$this->set('followings', $data);
	}

	/**
	 * View for the list of followers
	 * @param $id
	 */
	public function followers($id)
	{
		$this->Paginator->settings = array('limit' => 6,
			'conditions' => array(
				'Follower.followed_user_id' => $id,
				'Follower.status' => 1
			),
			'contain' => array(
				'User' => array(
					'ProfilePicture' => array(
						'order' => 'ProfilePicture.id DESC',
						'limit' => 1
					),
					'Followers' => array(
						'conditions' => array('Followers.user_id' => $this->Auth->User('id'))
					)
				),
				'FollowedUser' => array('ProfilePicture' => array(
					'order' => 'ProfilePicture.id DESC',
					'limit' => 1
				))
			)
		);

		$data = $this->Paginator->paginate('Follower');
		$this->set('followings', $data);
	}
}

