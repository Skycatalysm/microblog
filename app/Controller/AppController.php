<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package        app.Controller
 * @link        https://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
	public $helpers = array('Html', 'Form');

	/**
	 * Set $components variable defaults
	 * @var array
	 */
	public $components = array(
		'DebugKit.Toolbar',
		'Security' => array(
			'csrfExpires' => '+1 hour'
		),
		'Session',
		'Flash',
		'Auth' => array(
			'authenticate' => array(
				'Form' => array(
					'passwordHasher' => 'Blowfish'
				)
			),
			'authorize' => array('Controller'),
			'flash' => array(
				'params' => array(
					'class' => 'alert alert-warning'
				)
			)
		)
	);

	/**
	 * Set authorization
	 * @param $user
	 * @return bool
	 */
	public function isAuthorized($user)
	{
		// Admin can access every action
		if (isset($user['role']) && $user['role'] === 'admin') {
			return true;
		}

		// Default deny
		return false;
	}

	/**
	 * Check Authorization before filter
	 * Allow not logged in to view 'view, index'
	 */
	public function beforeFilter()
	{
		$id = $this->Auth->user('id');
		if (isset($id)) {
			$this->Auth->allow('view');
		}
		$this->Auth->allow('index');
		$this->Security->blackHoleCallback = 'blackhole';
	}

	/**
	 * check security
	 */
	public function blackhole($type)
	{
		$this->Flash->error(__('Please don\'t tamper the ' . $type . '.')
		);
		return $this->redirect(Router::url($this->referer(), true));
	}
}
