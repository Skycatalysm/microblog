<?php

class CommentsController extends AppController
{
	/**
	 * Add to components
	 * @var array
	 */
	public $components = array('Paginator');


	/**
	 * Check authorization
	 * @param $user
	 * @return bool
	 */
	public function isAuthorized($user)
	{
		if ($this->action === 'comment' || $this->action === 'ajaxComments') {
			return true;
		}

		// The routes accessible to the owner of the account
		if (in_array($this->action, array('delete'))) {
			$commentId = (int)$this->request->params['pass'][0];
			if ($this->Comment->isOwnedBy($commentId, $user['id'])) {
				return true;
			}
		}

		return parent::isAuthorized($user);
	}

	/**
	 * For adding comments
	 * @return CakeResponse|null
	 * @throws Exception
	 */
	public function comment()
	{
		if ($this->request->is('post')) {
			$this->request->data['Comment']['user_id'] = $this->Auth->User('id');
			$this->request->data['Comment']['status'] = 1;

			if ($this->Comment->save($this->request->data['Comment'])) {
				$this->Flash->success(__('Successfully commented.'));
				return $this->redirect(Router::url($this->referer(), true));
			}
			foreach ($this->Comment->validationErrors as $validationErrors) {
				foreach ($validationErrors as $validationError) {
					$this->Flash->error(__($validationError));
				}
			}
		}
		return $this->redirect(Router::url($this->referer(), true));
	}

	/**
	 * For deleting comments
	 * @param null $id
	 * @return CakeResponse|null
	 * @throws Exception
	 */
	public function delete($id = null)
	{
		if ($this->request->is('get')) {
			$conditions = array(
				'Comment.user_id' => $this->Auth->user('id'),
				'Comment.id' => $id
			);
			if ($this->Comment->hasAny($conditions)) {

				$comment = $this->Comment->find('first', array('conditions' => $conditions));
				$this->Comment->read('status', $comment['Comment']['id']);
				$this->Comment->set(array(
					'status' => 0
				));
				if ($this->Comment->save()) {
					$this->Flash->success(__('Successfully deleted a comment.'));
					return $this->redirect(Router::url($this->referer(), true));
				}
			}
		}
		return $this->redirect(Router::url($this->referer(), true));
	}

	/**
	 * @return array
	 */
	public function ajaxComments($id = null)
	{
		$this->autoRender = false;
		$this->Comment->recursive = -1;
		$this->Paginator->settings = array(
			'limit' => 3,
			'conditions' => array(
				'Comment.post_id' => $id,
				'Comment.status' => 1,
			),
			'order' => 'Comment.id ASC',
			'contain' => array(
				'User' => array(
					'ProfilePicture'
				)
			)
		);
		try {
			$data = $this->Paginator->paginate('Comment');
		} catch (NotFoundException $e) {
			$data = 'failed';
		}

		return json_encode($data);
	}
}

