<?php
// app/Controller/UsersController.php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class UsersController extends AppController
{
	public $components = array('Paginator');

	/**
	 * Check authorization
	 * @param $user
	 * @return bool
	 */
	public function isAuthorized($user)
	{
		if ($this->action === 'search') {
			return true;
		}

		// The routes accessible to the owner of the account
		if (in_array($this->action, array('update', 'changePassword', 'updateProfilePicture'))) {
			$profileId = (int)$this->request->params['pass'][0];
			if ((int)$this->Auth->User('id') === (int)$profileId) {
				return true;
			}
		}

		return parent::isAuthorized($user);
	}

	/**
	 * Allow some pages before filter
	 */
	public function beforeFilter()
	{
		parent::beforeFilter();
		// Allow users to register and logout.
		$this->Auth->allow('register', 'logout', 'verify');
	}

	/**
	 * User login
	 * @return CakeResponse|null
	 */
	public function login()
	{
		$id = $this->Auth->User('id');
		if (isset($id)) {
			return $this->redirect(array(
				'controller' => 'users',
				'action' => 'view',
				$id,
			));
		}

		if ($this->request->is('post')) {
			$conditions = array(
				'User.username' => $this->request->data['User']['username'],
				'User.status' => 0,
			);

			if ($this->User->hasAny($conditions)) {
				$this->Flash->error(__('Please confirm account through your email.'));
				return;
			}

			if ($this->Auth->login()) {
				$user = $this->User->findById($this->Auth->user('id'));
				$userPicture = (isset($user['ProfilePicture'][0]['image_name'])) ? $user['ProfilePicture'][0]['image_name'] : 'default_profile_picture.png';
				$this->Session->write('Auth.User.profile_picture', $userPicture);
				$userName = ucfirst($this->Auth->user('username'));
				$this->Flash->success(__('Welcome back ' . $userName . '!'));
				return $this->redirect(array(
					'controller' => 'users',
					'action' => 'view',
					$this->Auth->user('id'),
				));
			}
			$this->Flash->error(__('Invalid username or password, try again.'));
		}
	}

	/**
	 * User logout
	 * @return CakeResponse|null
	 */
	public function logout()
	{
		if ($this->Auth->logout()) {
			return $this->redirect(array(
				'controller' => 'users',
				'action' => 'login'
			));
		}
		return $this->redirect($this->Auth->logout());
	}

	/**
	 * Search profiles
	 */
	public function search()
	{
		$q = null;
		if ($this->request->is('get')) {
			$q = $this->request->query('q');
		}
		$conditions = array();
		$search_terms = explode(' ', $q);
		foreach ($search_terms as $search_term) {
			$conditions[] = array('User.username LIKE' => "%$search_term%");
			$conditions[] = array('User.first_name LIKE' => "%$search_term%");
			$conditions[] = array('User.last_name LIKE' => "%$search_term%");
			$conditions[] = array('User.email LIKE' => "%$search_term%");
		}
		$this->Paginator->settings = array(
			'limit' => 6,
			'conditions' => array(
				'OR' => $conditions
			),
			'contain' => array(
				'ProfilePicture' => array(
					'limit' => 1,
					'order' => 'ProfilePicture.created DESC',
				),
				'Followers'
			)
		);

		$data = $this->Paginator->paginate('User');
		$this->set('users', $data);
	}

	/**
	 * @param null $id
	 * @return CakeResponse|null
	 * @throws Exception
	 */
	public function view($id = null)
	{
		/**
		 * Check if user exists
		 */
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new MissingActionException(__('Invalid user'));
		}

		/**
		 * Create Post if its set
		 */
		$this->loadModel('Post');
		if ($this->request->is('post') || $this->request->is('put')) {
			if (isset($this->data['Post']['image']['size'])) {
				if (!$this->data['Post']['image']['size'] > 0 && !empty($this->data['Post']['image']['name'])) {
					$this->Flash->error(
						__('File too large for upload.')
					);
					return $this->redirect(Router::url($this->referer(), true));
				}
				if ($this->data['Post']['image']['size'] > 2000000) {
					$this->Flash->error(
						__('File too large for upload.')
					);
					return $this->redirect(Router::url($this->referer(), true));
				}
				if ($this->data['Post']['image']['size'] > 0) {
					if (!empty($this->data['Post']['image']['name'])) {
						$file = $this->data['Post']['image']; //put the data into a var for easy use
						$ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension

						$arr_ext = array('jpg', 'jpeg', 'png', 'gif'); //set allowed extensions

						$file['name'] = uniqid() . '.' . $ext;

						//only process if the extension is valid
						if (in_array($ext, $arr_ext)) {
							//do the actual uploading of the file. First arg is the tmp name, second arg is
							//where we are putting it
							move_uploaded_file($file['tmp_name'], WWW_ROOT . '/img/post/' . $file['name']);

							$this->request->data['Post']['image_name'] = $file['name'];
						} else {
							$this->Flash->error(
								__('Image extension "' . $ext . '" not valid. Please, try again.')
							);
							return $this->redirect(Router::url($this->referer(), true));
						}
					}
				}
			}
			//Added this line
			$this->request->data['Post']['user_id'] = $this->Auth->user('id');
			if ($this->Post->save($this->request->data)) {
				$this->Flash->success(__('Your post has been saved.'));
				return $this->redirect(Router::url($this->referer(), true));
			}
		}

		/**
		 * Find the owner of profile and render it
		 */
		$user = $this->User->find('first', array(
			'conditions' => array('User.id' => $id),
			'contain' => array(
				'ProfilePicture' => array(
					'limit' => 1,
					'order' => 'ProfilePicture.id DESC',
				),
				'Followings',
				'Followers',
				'Post' => array(
					'conditions' => array('status' => 1),
					'order' => 'Post.id DESC',
					'SharedPost' => array(
						'User' => array(
							'ProfilePicture'
						)
					),
					'Like',
					'Comment' => array(
						'User' => array(
							'ProfilePicture'
						)
					)
				)
			),
		));
		$this->set('user', $user);
	}

	/**
	 * User registration
	 * @return CakeResponse|null
	 * @throws Exception
	 */
	public function register()
	{
		$id = $this->Auth->User('id');
		if (isset($id)) {
			return $this->redirect(array(
				'controller' => 'users',
				'action' => 'view',
				$id,
			));
		}
		if ($this->request->is('post')) {
			$this->User->create();
			$this->request->data['User']['email_hash'] = Security::hash(uniqid());

			if ($this->User->save($this->request->data)) {
				$Email = new CakeEmail();
				$Email->template('verification', null);
				$Email->emailFormat('html');
				$Email->config('gmail');
				$Email->from(array('microblogcatalyst@gmail.com' => 'Microblog'));
				$Email->to($this->request->data['User']['email']);
				$Email->subject('Email verification');
				if ($Email->send($this->request->data['User']['email_hash'])) {
					$this->Flash->success(__(
						'Registration complete! Email verification sent! Please check your email and confirm.'));
					return $this->redirect(array('controller' => 'users', 'action' => 'login'));
				}
				$this->Flash->error(__('Registration complete but email failed. Please click resend in a few moments')
				);
			}
			$this->Flash->error(
				__('Registration failed. Please, try again.')
			);
		}
	}

	/**
	 * User update basic information
	 * @param null $id
	 * @return CakeResponse|null
	 * @throws Exception
	 */
	public function update($id = null)
	{
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new MissingActionException(__('Invalid user'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->User->save($this->request->data)) {
				$user = $this->User->findById($id);
				$this->Auth->login($user['User']);
				$userPicture = (isset($user['ProfilePicture'][0]['image_name'])) ? $user['ProfilePicture'][0]['image_name'] : 'default_profile_picture.png';
				$this->Session->write('Auth.User.profile_picture', $userPicture);
				$this->Flash->success(__('Information saved!'));
				return $this->redirect(array(
						'controller' => 'users',
						'action' => 'view',
						$this->Auth->user('id')
					)
				);
			}
			$this->Flash->error(
				__('Information could not be saved. Please, try again.')
			);
		} else {
			$this->request->data = $this->User->findById($id);
			unset($this->request->data['User']['password']);
		}
	}


	/**
	 * Change user password
	 * @param null $id
	 * @return CakeResponse|null
	 * @throws Exception
	 */
	public function changePassword($id = null)
	{
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new MissingActionException(__('Invalid user'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->User->save($this->request->data)) {
				$this->Flash->success(__('The password has been changed.'));
				return $this->redirect(array(
						'controller' => 'users',
						'action' => 'view',
						$this->Auth->user('id'))
				);
			}
			$this->Flash->error(
				__('Password could not be changed. Please, try again.')
			);
		} else {
			$this->request->data = $this->User->findById($id);
			unset($this->request->data['User']['password']);
		}
	}

	/**
	 * Verify account from email
	 * @param $hash
	 * @return CakeResponse|null
	 * @throws Exception
	 */
	public function verify($hash)
	{
		$conditions = array(
			'User.email_hash' => $hash,
			'User.status' => 0
		);
		if ($this->User->hasAny($conditions)) {

			$user = $this->User->findByEmailHash($hash);
			$this->User->read('status', $user['User']['id']);
			$this->User->set(array(
				'status' => 1
			));
			if (!$this->User->save()) {
				$this->Flash->error(__('Something went wrong. Please contact your developer.'));
				return $this->redirect(array(
					'controller' => 'users',
					'action' => 'login'
				));
			}

			if ($this->Auth->login($user['User'])) {
				$userPicture = 'default_profile_picture.png';
				$this->Session->write('Auth.User.profile_picture', $userPicture);
				$userName = ucfirst($this->Auth->user('username'));
				$this->Flash->success(__('Your account is now verified! Welcome ' . $userName . '!'));
				return $this->redirect(array(
					'controller' => 'users',
					'action' => 'view',
					$this->Auth->user('id'),
				));
			}
		}
		return $this->redirect(array(
			'controller' => 'users',
			'action' => 'login'
		));
	}
}
