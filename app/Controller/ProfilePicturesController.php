<?php

class ProfilePicturesController extends AppController
{
	/**
	 * Check Authorization
	 * @param $user
	 * @return bool
	 */
	public function isAuthorized($user)
	{
		// The owner of a profile picture can update it
		if (in_array($this->action, array('update'))) {
			$pictureOwner = (int)$this->request->params['pass'][0];
			if ((int)$pictureOwner === (int)$user['id']) {
				return true;
			}
		}

		return parent::isAuthorized($user);
	}

	/**
	 * For updating profile picture
	 * @param null $id
	 * @return CakeResponse|null
	 * @throws Exception
	 */
	public function update($id = null)
	{
		$this->loadModel('User');

		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}

		$this->ProfilePicture->recursive = -1;
		$ProfilePicture = $this->ProfilePicture->findByUserId($id, array(), array('ProfilePicture.created' => 'desc'));
		$this->set($ProfilePicture);


		if ($this->request->is('post') || $this->request->is('put')) {
			if (!$this->data['ProfilePicture']['image']['size'] > 0 && !empty($this->data['ProfilePicture']['image']['name'])) {
				$this->Flash->error(
					__('File too large for upload.')
				);
				return $this->redirect(Router::url($this->referer(), true));
			}
			if ($this->data['ProfilePicture']['image']['size'] > 2000000) {
				$this->Flash->error(
					__('File too large for upload.')
				);
				return $this->redirect(Router::url($this->referer(), true));
			}
			if (!$this->data['ProfilePicture']['image']['size'] > 0) {
				$this->Flash->error(
					__('Please select an image.')
				);
				return $this->redirect(array(
						'controller' => 'profilePictures',
						'action' => 'update',
						$this->Auth->user('id'))
				);
			}
			$this->request->data['ProfilePicture']['user_id'] = $this->Auth->user('id');
			//Check if image has been uploaded
			if (!empty($this->data['ProfilePicture']['image']['name'])) {
				$file = $this->data['ProfilePicture']['image']; //put the data into a var for easy use
				$ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension

				$arr_ext = array('jpg', 'jpeg', 'gif', 'png'); //set allowed extensions
				$file['name'] = uniqid() . '.' . $ext;

				//only process if the extension is valid
				if (in_array($ext, $arr_ext)) {
					//do the actual uploading of the file. First arg is the tmp name, second arg is
					//where we are putting it
					move_uploaded_file($file['tmp_name'], WWW_ROOT . '/img/profile/' . $file['name']);

					$this->request->data['ProfilePicture']['image_name'] = $file['name'];
				} else {
					$this->Flash->error(
						__('Profile picture extension "' . $ext . '" not valid. Please, try again.')
					);
					return $this->redirect(array(
							'controller' => 'profilePictures',
							'action' => 'update',
							$this->Auth->user('id'))
					);
				}
			}
			if ($this->ProfilePicture->save($this->request->data)) {
				$this->Flash->success(__('Profile picture updated.'));
				$this->loadModel('User');
				$user = $this->User->findById($this->Auth->user('id'));
				$userPicture = (isset($user['ProfilePicture'][0]['image_name'])) ? $user['ProfilePicture'][0]['image_name'] : 'default_profile_picture.png';
				$this->Session->delete('Auth.User.profile_picture');
				$this->Session->write('Auth.User.profile_picture', $userPicture);
				return $this->redirect(array(
						'controller' => 'users',
						'action' => 'view',
						$this->Auth->user('id'))
				);
			}
			$this->Flash->error(
				__('Profile picture could not be updated. Please, try again.')
			);
		}

	}
}

