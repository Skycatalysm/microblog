<?php
// app/Controller/UsersController.php
App::uses('AppController', 'Controller');

class PostsController extends AppController
{
	public $components = array('Paginator');

	/**
	 * Check authorization
	 * @param $user
	 * @return bool
	 */
	public function isAuthorized($user)
	{

		if ($this->action === 'share' || $this->action === 'feed' || $this->action === 'edit') {
			return true;
		}

		// The routes accessible to the owner of the account
		if (in_array($this->action, array('delete'))) {
			$postId = (int)$this->request->params['pass'][0];
			if ($this->Post->isOwnedBy($postId, $user['id'])) {
				return true;
			}
		}

		return parent::isAuthorized($user);
	}

	public function beforeFilter()
	{
		$this->Auth->allow('feed');
		parent::beforeFilter();
	}

	/**
	 * View for the news feed
	 */
	public function feed()
	{
		$id = $this->Auth->User('id');

		if (!isset($id)) {
			return $this->redirect(array('controller' => 'users', 'action' => 'login'));
		}

		$this->loadModel('Follower');
		$this->Follower->recursive = -1;
		$followedUser = $this->Follower->find('all', array(
			'conditions' => array(
				'AND' => array(
					'Follower.user_id' => $this->Auth->User('id'),
					'Follower.status' => 1
				)
			)
		));

		$conditions = array();
		foreach ($followedUser as $value) {
			$conditions[] = array('Post.user_id' => $value['Follower']['followed_user_id']);
		}
		$conditions[] = array('Post.user_id' => $this->Auth->User('id'));

		$this->Paginator->settings = array(
			'limit' => 3,
			'contain' => array(
				'SharedPost' => array(
					'User' => array(
						'ProfilePicture'
					)
				),
				'User' => array(
					'ProfilePicture'
				),
				'Like',
				'Comment' => array(
					'User'
				),
			),
			'conditions' => array(
				'OR' => $conditions,
				'Post.status' => 1
			),
			'order' => 'Post.id DESC'
		);

		$data = $this->Paginator->paginate('Post');
		$this->set('posts', $data);
	}

	/**
	 * Function for sharing posts
	 * @return CakeResponse|null
	 * @throws Exception
	 */
	public function share()
	{
		if ($this->request->is('post')) {
			if (!$this->Post->exists($this->request->data['Post']['shared_post_id'])) {
				$this->Flash->error(__('You can\'t share something that doesn\'t exist.'));
				return $this->redirect(Router::url($this->referer(), true));
			}
			$this->request->data['Post']['user_id'] = $this->Auth->user('id');
			if ($this->Post->save($this->request->data)) {
				$this->Flash->success(__('You have shared a post.'));
			}
			foreach ($this->Post->validationErrors as $validationErrors) {
				foreach ($validationErrors as $validationError) {
					$this->Flash->error(__($validationError));
				}
			}
		}
		return $this->redirect(Router::url($this->referer(), true));
	}


	/**
	 * For Adding posts
	 * @return CakeResponse|null
	 * @throws Exception
	 */
	public function add()
	{
		if ($this->request->is('post')) {
			//Added this line
			$this->request->data['Post']['user_id'] = $this->Auth->user('id');
			if ($this->Post->save($this->request->data)) {
				$this->Flash->success(__('Your post has been saved.'));
				return $this->redirect(Router::url($this->referer(), true));
			}
			foreach ($this->Post->validationErrors as $validationErrors) {
				foreach ($validationErrors as $validationError) {
					$this->Flash->error(__($validationError));
				}
			}
			return $this->redirect(Router::url($this->referer(), true));
		}
	}

	/**
	 * Edit a post based on specific id
	 * @param null $id
	 * @return CakeResponse|null
	 * @throws Exception
	 */
	public function edit()
	{
		$id = $this->request->data['Post']['id'];
		if (!$id) {
			throw new NotFoundException(__('Invalid post'));
		}

		if (!$this->Post->isOwnedBy($id, $this->Auth->User('id'))) {
			$this->Flash->error('You can\'t edit someone else\'s post.');
			return $this->redirect(Router::url($this->referer(), true));
		}

		$post = $this->Post->findById($id);
		if (!$post) {
			throw new NotFoundException(__('Invalid post'));
		}

		if ($this->request->is(array('post', 'put'))) {
			$this->Post->id = $id;

			if ($this->Post->save($this->request->data)) {
				// In your Controller
				$this->Flash->success('The post has been updated.');
				return $this->redirect(Router::url($this->referer(), true));
			}
			$this->Flash->error(__('Unable to update your post.'));
		}

		foreach ($this->Post->validationErrors as $validationErrors) {
			foreach ($validationErrors as $validationError) {
				$this->Flash->error(__($validationError));
			}
		}

		if (!$this->request->data) {
			$this->request->data = $post;
		}

		return $this->redirect(Router::url($this->referer(), true));
	}

	/**
	 * Delete a post based on a specific id
	 * @param null $id
	 * @return CakeResponse|null
	 * @throws Exception
	 */
	public function delete($id = null)
	{
		if ($this->request->is('get')) {
			$conditions = array(
				'Post.user_id' => $this->Auth->user('id'),
				'Post.id' => $id
			);
			if ($this->Post->hasAny($conditions)) {

				$post = $this->Post->find('first', array('conditions' => $conditions));
				$this->Post->read('status', $post['Post']['id']);
				$this->Post->set(array(
					'status' => 0
				));
				if ($this->Post->save()) {
					$this->Flash->success(__('Your post has been deleted.'));
					return $this->redirect(Router::url($this->referer(), true));
				}
			}
		}
		return $this->redirect(Router::url($this->referer(), true));
	}
}

