<div class="row justify-content-md-center">
	<div class="col-md-12">
		<?php echo $this->Flash->render('auth'); ?>
		<?php echo $this->Form->create('User'); ?>
		<h5 class="font-weight-bold text-center">REGISTER</h5>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<?php echo $this->Form->create('User'); ?>
			<?php
			echo $this->Form->input(
				'username',
				array(
					'class' => 'form-control',
					'placeholder' => 'Enter username',
					'error' => array('attributes' => array('class' => 'text-danger'))
				)
			);
			?>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<?php
			echo $this->Form->input(
				'email',
				array(
					'class' => 'form-control',
					'placeholder' => 'Enter email',
					'error' => array('attributes' => array('class' => 'text-danger'))
				)
			);
			?>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<?php
			echo $this->Form->input(
				'password',
				array(
					'class' => 'form-control',
					'placeholder' => 'Password',
					'error' => array('attributes' => array('class' => 'text-danger'))
				)
			);

			?>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<?php
			echo $this->Form->input(
				'confirmPassword',
				array(
					'label' => 'Confirm Password',
					'class' => 'form-control',
					'placeholder' => 'Re-enter password',
					'type' => 'password'
				)
			);
			?>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<?php
			echo $this->Form->input(
				'first_name',
				array(
					'label' => 'First Name',
					'class' => 'form-control',
					'placeholder' => 'Enter first name',
					'type' => 'text',
					'error' => array('attributes' => array('class' => 'text-danger'))
				)
			);
			?>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<?php
			echo $this->Form->input(
				'last_name',
				array(
					'label' => 'Last Name',
					'class' => 'form-control',
					'placeholder' => 'Enter last name',
					'type' => 'text',
					'error' => array('attributes' => array('class' => 'text-danger'))
				)
			);
			?>
		</div>
	</div>
	<div class="col-md-12">
		<div class="text-center">
			<?php echo $this->Form->button(__('Register'), array('class' => 'btn btn-primary px-5')); ?>
		</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
