<div class="row justify-content-md-center">
	<div class="col-md-12">
		<?php echo $this->Flash->render('auth'); ?>
		<?php echo $this->Form->create('User'); ?>
		<h5 class="font-weight-bold text-center">Change Password</h5>
	</div>
</div>
<div class="row justify-content-md-center">
	<div class="col-md-6">
		<div class="form-group">
			<?php
			echo $this->Form->input(
				'password',
				array(
					'class' => 'form-control',
					'placeholder' => 'Password',
					'error' => array('attributes' => array('class' => 'text-danger'))
				)
			);

			?>
		</div>
	</div>
</div>
<div class="row justify-content-md-center">
	<div class="col-md-6">
		<div class="form-group">
			<?php
			echo $this->Form->input(
				'confirmPassword',
				array(
					'label' => 'Confirm Password',
					'class' => 'form-control',
					'placeholder' => 'Re-enter password',
					'type' => 'password'
				)
			);
			?>
		</div>
	</div>
</div>
<div class="row justify-content-md-center">
	<div class="col-md-12">
		<div class="text-center">
			<?php echo $this->Form->button(__('Change password'), array('class' => 'btn btn-primary px-5')); ?>
		</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
</div>
