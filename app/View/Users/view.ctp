<div class="row">
	<div class="col-12">
	</div>
	<div class="col-6">
		<div class="row">
			<div class="col-md-6 col-lg-4">
				<div class="profile-picture-container border border-light">
					<?php
					$pictureName = 'default_profile_picture.png';
					if (isset($user['ProfilePicture'][0]['image_name'])) {
						$pictureName = $user['ProfilePicture'][0]['image_name'];
					}
					echo $this->Html->image(
						'profile/' . $pictureName, array(
							'alt' => 'Profile-picture',
							'class' => 'img-fluid')
					); ?>
				</div>
			</div>
			<div class="col-md-6">
				<div class="d-flex align-items-start flex-column h-100">
					<div class="mb-auto">
						<h3>
							<?php echo nl2br(h($user['User']['first_name'] . ' ' . $user['User']['last_name'])); ?>
						</h3>
						<h5 class="text-success">
							<?php echo nl2br(h($user['User']['email'])); ?>
						</h5>
					</div>
					<div>
						<b>
							<?php
							echo
							$this->Html->link(
								__('Followers:'),
								array('controller' => 'followers', 'action' => 'followers', $user['User']['id']),
								array('class' => 'text-primary')
							)
							?>
						</b>
						<span class="text-success">
							<?php echo count($user['Followers']); ?>
						</span>
						&nbsp; &nbsp;
						<b>
							<?php
							echo
							$this->Html->link(
								__('Followings:'),
								array('controller' => 'followers', 'action' => 'followings', $user['User']['id']),
								array('class' => 'text-primary')
							)
							?>
						</b>
						<span class="text-success">
							<?php echo count($user['Followings']); ?>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-6 text-right">
		<div class="mr-3 d-flex align-items-end flex-column h-100">
			<div>
				<b>Joined Microblog</b>
			</div>
			<div class="mb-auto">
				<small class="text-secondary">
					<?php echo $this->Time->niceShort($user['User']['created']); ?>
				</small>
			</div>
			<?php
			$following = false;
			foreach ($user['Followers'] as $follower) {
				if ((int)$follower['user_id'] == (int)$this->Session->read('Auth.User.id')) {
					$following = true;
					break;
				}
			}
			?>
			<?php if ($user['User']['id'] === $this->Session->read('Auth.User.id')): ?>
			<?php elseif ($following): ?>
				<?php echo $this->Form->create('follower', array(
					'url' => array('controller' => 'followers', 'action' => 'unfollow')
				));
				?>
				<?php echo $this->Form->input('followed_user_id', array(
					'type' => 'hidden',
					'value' => $user['User']['id']
				));
				?>
				<?php echo $this->Form->button(__('unfollow'), array('class' => 'btn btn-outline-info px-3')); ?>
				<?php echo $this->Form->end(); ?>
			<?php else: ?>
				<?php echo $this->Form->create('follower', array(
					'url' => array('controller' => 'followers', 'action' => 'follow')
				));
				?>
				<?php echo $this->Form->input('followed_user_id', array(
					'type' => 'hidden',
					'value' => $user['User']['id']
				));
				?>
				<?php echo $this->Form->button(__('follow'), array('class' => 'btn btn-primary px-3')); ?>
				<?php echo $this->Form->end(); ?>
			<?php endif ?>
		</div>
	</div>
</div>
<hr>
<?php if ($user['User'] ['id'] === $this->Session->read('Auth.User.id')): ?>
	<div class="p-3 rounded border border-secondary shadow">
		<?php echo $this->Form->create('Post', array('enctype' => 'multipart/form-data')); ?>
		<b><?php echo __('Create'); ?><?php echo ' '; ?><?php echo __('Post'); ?></b>
		<?php
		echo $this->Form->input(
			'content',
			array(
				'label' => false,
				'rows' => 3,
				'class' => 'form-control',
				'placeholder' => 'Say something...',
				'error' => array('attributes' => array('class' => 'text-danger'))
			)
		);
		echo $this->Form->input(
			'image',
			array(
				'label' => false,
				'class' => 'form-control mt-2',
				'placeholder' => 'Enter username',
				'error' => array('attributes' => array('class' => 'text-danger')),
				'type' => 'file'
			)
		);
		?>
		<div class="text-right mt-2">
			<?php echo $this->Form->button(__('Post'), array('class' => 'btn btn-outline-success')); ?>
		</div>
		<?php echo $this->Form->end(); ?>
	</div>
<?php endif; ?>

<?php foreach ($user['Post'] as $post): ?>
	<div class="row my-3">
		<div class="col-12">
			<div class="p-3 rounded border border-secondary shadow">
				<div class="d-flex flex-row">
					<div class="pl-2 pt-2">
						<div class="profile-picture-post-container">
							<?php
							echo $this->Html->image(
								'profile/' . $pictureName, array(
									'alt' => 'Profile-picture')
							); ?>
						</div>
					</div>
					<div class="p-2 w-100">
						<div class="row pl-2">
							<div class="col-6">
								<span class="font-weight-bold">
								<?php
								echo $this->Html->link(
									$user['User']['first_name'] . ' ' . $user['User']['last_name'],
									array('controller' => 'users', 'action' => 'view', $user['User']['id']),
									array('class' => 'font-weight-bold text-primary')
								)
								?>
								</span>
								<br>
								<small class="text-success">
									<?php echo nl2br(h($user['User']['email'])) ?>
								</small>
							</div>
							<div class="col-6 text-right">
								<?php if ($post['user_id'] === $this->Session->read('Auth.User.id')): ?>
									<!-- Button trigger modal -->
									<a onclick="event.preventDefault();" href="#"
									   class="mr-2 text-success edit-post-modal-trigger"
									   data-target="#editPostModal" data-post-id="<?php echo $post['id'] ?>">edit</a>
									<?php
									echo $this->Html->link(
										'×',
										array('controller' => 'posts', 'action' => 'delete', $post['id']),
										array(
											'class' => 'close text-secondary delete-post-modal-trigger',
											'onclick' => 'event.preventDefault();',
											'data-target' => '#deletePostModal',
											'data-post-id' => '#post-id' . $post['id']
										)
									)
									?>
								<?php endif ?>
							</div>
							<div id="<?php echo 'post-id' . $post['id']; ?>" class="col-12 pt-2 my-2">
								<span id="<?php echo 'post-content' . $post['id']; ?>" class="text-black">
									<?php echo nl2br(h($post['content'])); ?>
								</span>
								<div id="<?php echo 'post-image' . $post['id']; ?>">
									<?php
									if (!empty($post['image_name'])) {
										echo $this->Html->image(
											'post/' . $post['image_name'], array(
												'alt' => 'Post-picture',
												'class' => 'img-fluid rounded mx-auto d-block my-3')
										);
									}
									?>
								</div>
								<br>
								<small class="text-secondary">
									(<?php echo $this->Time->niceShort($post['created']); ?>)
								</small>
								<?php
								$sharedPictureName = 'default_profile_picture.png';
								if (isset($post['SharedPost']['User']['ProfilePicture'][0]['image_name'])) {
									$sharedPictureName = $post['SharedPost']['User']['ProfilePicture'][0]['image_name'];
								}
								$sharedPostStatus = (isset($post['SharedPost']['status'])) ? (int)$post['SharedPost']['status'] : null;
								?>
								<div id="<?php echo 'post-rendered' . $post['id']; ?>">
									<?php if ($sharedPostStatus === 1): ?>
										<div class="bg-light p-3 rounded">
											<div class="bg-light rounded p-3">
												<div class="d-flex flex-row">
													<div class="pl-2 pt-2">
														<div class="profile-picture-post-container">
															<?php
															echo $this->Html->image(
																'profile/' . $sharedPictureName, array(
																	'alt' => 'Profile-picture')
															); ?>
														</div>
													</div>
													<div class="p-2 w-100">
														<div class="row pl-2">
															<div class="col-6">
																<?php
																echo $this->Html->link(
																	$post['SharedPost']['User']['first_name'] . ' ' . $post['SharedPost']['User']['last_name'],
																	array('controller' => 'users', 'action' => 'view', $post['SharedPost']['User']['id']),
																	array('class' => 'font-weight-bold text-primary')
																)
																?>
																<br>
																<small class="text-success">
																	<?php echo nl2br(h($post['SharedPost']['User']['email'])) ?>
																</small>
															</div>
															<div class="col-12 pt-2 my-2">
																	<span class="text-black">
																		<?php echo nl2br(h($post['SharedPost']['content'])); ?>
																	</span>
																<?php
																if (!empty($post['SharedPost']['image_name'])) {
																	echo $this->Html->image(
																		'post/' . $post['SharedPost']['image_name'], array(
																			'alt' => 'Post-picture',
																			'class' => 'img-fluid rounded mx-auto d-block my-2')
																	);
																}
																?>
																<br>
																<small class="text-secondary">
																	(<?php echo $this->Time->niceShort($post['SharedPost']['created']); ?>
																	)
																</small>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									<?php elseif ($sharedPostStatus === 0): ?>
										<div class="border border-info p-4 rounded">
											<strong>
												Content not available.
											</strong><br>
											This post has been deleted.
										</div>
									<?php endif ?>
								</div>
							</div>
							<?php
							//Define the contents to be shared
							$shareId = $post['id'];
							$sharePicture = $pictureName;
							$shareFullName = $user['User']['first_name'] . ' ' . $user['User']['last_name'];
							$shareUserId = $user['User']['id'];
							$shareContent = $post['content'];
							$shareEmail = $user['User']['email'];
							$shareCreatedDate = $post['created'];
							if (isset($post['SharedPost']['id'])) {
								$shareId = $post['SharedPost']['id'];
								$sharePicture = $sharedPictureName;
								$shareFullName = $post['SharedPost']['User']['first_name'] . ' ' . $post['SharedPost']['User']['last_name'];
								$shareUserId = $post['SharedPost']['User']['id'];
								$shareContent = $post['SharedPost']['content'];
								$shareEmail = $post['SharedPost']['User']['email'];
								$shareCreatedDate = $post['SharedPost']['created'];
							}
							$sharedImage = null;
							if (!empty($post['image_name'])) {
								$sharedImage = $this->Html->image(
									'post/' . $post['image_name'], array(
										'alt' => 'Post-picture',
										'class' => 'img-fluid rounded mx-auto d-block my-2')
								);
							}
							if (!empty($post['SharedPost']['image_name'])) {
								$sharedImage = $this->Html->image(
									'post/' . $post['SharedPost']['image_name'], array(
										'alt' => 'Post-picture',
										'class' => 'img-fluid rounded mx-auto d-block my-2')
								);
							}
							?>
							<div class="col-12 pt-2 mb-0 pb-0">
								<small id="likeCount<?php echo $post['id']; ?>" class="text-info">
									<?php
									if (count($post['Like']) === 1) {
										echo count($post['Like']) . ' like.';
									} elseif (count($post['Like']) > 1) {
										echo count($post['Like']) . ' likes.';
									}
									?>
								</small>
								<div class="d-flex flex-row mb-3">
									<div>
										<?php
										$liked = 'like';
										foreach ($post['Like'] as $like) {
											if ((int)$like['user_id'] == (int)$this->Session->read('Auth.User.id')) {
												$liked = 'unlike';
												break;
											}
										}
										?>
										<button class="btn btn-sm px-3 btn-outline-success ajaxLike"
												data-liked-id="<?php echo $post['id']; ?>">
											<?php
											echo $liked;
											?>
										</button>
									</div>
									<div>
										<!-- Button trigger modal -->
										<button type="button"
												class="btn btn-sm px-3 btn-outline-success ml-2 share-post-modal-trigger"
												data-target="#sharePostModal" data-post-id="<?php echo $shareId; ?>">
											share
										</button>
									</div>
								</div>
								<div class="d-none" id="sharing-post-content<?php echo $shareId; ?>">
									<?php if ($sharedPostStatus === 1 || $sharedPostStatus === null): ?>
										<div class="bg-light rounded p-3">
											<div class="d-flex flex-row">
												<div class="pl-2 pt-2">
													<div class="profile-picture-post-container">
														<?php
														echo $this->Html->image(
															'profile/' . $sharePicture, array(
																'alt' => 'Profile-picture')
														); ?>
													</div>
												</div>
												<div class="p-2 w-100">
													<div class="row pl-2">
														<div class="col-6">
															<?php
															echo $this->Html->link(
																$shareFullName,
																array('controller' => 'users', 'action' => 'view', $shareUserId),
																array('class' => 'font-weight-bold text-primary')
															)
															?>
															<br>
															<small class="text-success">
																<?php echo nl2br(h($shareEmail)); ?>
															</small>
														</div>
														<div class="col-12 pt-2 my-2">
																	<span class="text-black">
																		<?php echo nl2br(h($shareContent)); ?>
																	</span>
															<?php echo $sharedImage; ?>
															<br>
															<small class="text-secondary">
																(<?php echo $this->Time->niceShort($shareCreatedDate); ?>
																)
															</small>
														</div>
													</div>
												</div>
											</div>
										</div>
									<?php elseif ($sharedPostStatus === 0): ?>
										<div class="border border-info p-4 rounded">
											<strong>
												Content not available.
											</strong><br>
											This post has been deleted.
										</div>
									<?php endif ?>
								</div>
								<div class="border border-light shadow-sm p-3 mb-5 bg-white rounded">
									<?php if ($this->Session->read('Auth.User')) : ?>
										<div class="d-flex flex-row">
											<div class="m-1">
												<div class="profile-picture-comment-container">
													<?php
													echo $this->Html->image(
														'profile/' . $this->Session->read('Auth.User.profile_picture'), array(
															'alt' => 'Profile-picture',
															'class' => 'img-fluid')
													);
													?>
												</div>
											</div>
											<div class="w-100 m-1">
												<?php echo $this->Form->create('Comment', array(
													'url' => array('controller' => 'comments', 'action' => 'comment')
												));
												?>
												<?php echo $this->Form->input('post_id', array(
													'type' => 'hidden',
													'value' => $post['id']
												));
												?>
												<?php echo $this->Form->input('content', array(
													'rows' => '2',
													'label' => false,
													'class' => 'form-control',
													'placeholder' => 'Say something...'
												));
												?>
												<div class="text-right my-2">
													<?php echo $this->Form->button(__('submit'), array('class' => 'btn btn-outline-success btn-sm')); ?>
												</div>
												<?php echo $this->Form->end(); ?>
											</div>
										</div>
									<?php endif ?>
									<?php if (count($post['Comment']) > 0): ?>
										Comments:
										<div id="clone-comment<?php echo $post['id']; ?>">
										</div>
										<span data-show-comments-value="1"
											  data-show-comments-target="<?php echo $post['id']; ?>"
											  class="ajax-comments pointer-on-hover text-info">Load comments...
										</span>
									<?php endif ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endforeach; ?>
<?php
echo $this->element('Modal/delete_post');

echo $this->element('Modal/delete_comment');

echo $this->element('Modal/edit_post');

echo $this->element('Modal/share_post');

echo $this->element('comments/show_comments');
?>
