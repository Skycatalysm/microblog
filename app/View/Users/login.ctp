<div class="row justify-content-md-center">
	<div class="col-md-6">
		<?php echo $this->Flash->render('auth'); ?>
		<?php echo $this->Form->create('User'); ?>
		<h5 class="font-weight-bold text-center">LOGIN</h5>
		<div class="form-group">
			<?php
			echo $this->Form->input(
				'username',
				array('class' => 'form-control', 'placeholder' => 'Enter username')
			);
			?>
		</div>
		<div class="form-group">
			<?php
			echo $this->Form->input(
				'password',
				array(
					'class' => 'form-control',
					'placeholder' => 'Password')
			);
			?>
		</div>
		<div class="text-center">
			<?php echo $this->Form->button(__('Login'), array('class' => 'btn btn-primary px-5')); ?>
		</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
