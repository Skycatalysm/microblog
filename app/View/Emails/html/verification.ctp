<?php
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

$parsedUrl = parse_url($actual_link);
$root = strstr($actual_link, $parsedUrl['path'], true) . '/';
?>
<div style="border: solid #2c3e50;padding: 5rem;">
	<h3>Welcome to microblog!</h3>
	<p style="color:#18bc9c"><b>Before you can get your all-access pass to our service you need to confirm your account.
			Click below to confirm. </b></p>
	<a href="<?php echo $root ?>users/verify/<?php echo $content; ?>">
		<button
			style="border-radius: 5px;background-color: #2c3e50;border: none;color: white;padding: 15px 32px;text-align: center;text-decoration: none;display: inline-block;font-size: 16px;margin: 4px 2px;cursor: pointer;">
			Confirm Account
		</button>
	</a>
</div>
