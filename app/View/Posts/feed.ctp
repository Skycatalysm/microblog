<div class="text-center">
	<h5>
		News Feed
		<small class="text-dark ml-1">(<?php echo $this->Paginator->params()['count']; ?>)</small>
	</h5>
</div>
<div class="d-flex flex-row">
	<div class="mr-auto">
		<?php echo $this->Paginator->numbers(); ?>
	</div>
	<div class="text-success">
		page <?php echo $this->Paginator->counter(); ?>
	</div>
</div>
<?php foreach ($posts as $post): ?>
	<div class="row my-3">
		<div class="col-12">
			<div class="p-3 rounded border border-secondary shadow">
				<div class="d-flex flex-row">
					<div class="pl-2 pt-2">
						<div class="profile-picture-post-container">
							<?php
							$pictureName = 'default_profile_picture.png';
							if (isset($post['User']['ProfilePicture'][0]['image_name'])) {
								$pictureName = $post['User']['ProfilePicture'][0]['image_name'];
							}
							echo $this->Html->image(
								'profile/' . $pictureName, array(
									'alt' => 'Profile-picture')
							); ?>
						</div>
					</div>
					<div class="p-2 w-100">
						<div class="row pl-2">
							<div class="col-6">
								<span class="font-weight-bold">
								<?php
								echo $this->Html->link(
									$post['User']['first_name'] . ' ' . $post['User']['last_name'],
									array('controller' => 'users', 'action' => 'view', $post['User']['id']),
									array('class' => 'font-weight-bold text-primary')
								)
								?>
								</span>
								<br>
								<small class="text-success">
									<?php echo nl2br(h($post['User']['email'])) ?>
								</small>
							</div>
							<div class="col-6 text-right">
								<?php if ($post['Post']['user_id'] === $this->Session->read('Auth.User.id')): ?>
									<!-- Button trigger modal -->
									<a onclick="event.preventDefault();" href="#"
									   class="mr-2 text-success edit-post-modal-trigger"
									   data-target="#editPostModal" data-post-id="<?php echo $post['Post']['id'] ?>">edit</a>
									<?php
									echo $this->Html->link(
										'×',
										array('controller' => 'posts', 'action' => 'delete', $post['Post']['id']),
										array(
											'class' => 'close text-secondary delete-post-modal-trigger',
											'onclick' => 'event.preventDefault();',
											'data-target' => '#deletePostModal',
											'data-post-id' => '#post-id' . $post['Post']['id']
										)
									)
									?>
								<?php endif ?>
							</div>
							<div id="<?php echo 'post-id' . $post['Post']['id']; ?>" class="col-12 pt-2 my-2">
								<span id="<?php echo 'post-content' . $post['Post']['id']; ?>" class="text-black">
									<?php echo nl2br(h($post['Post']['content'])); ?>
								</span>
								<div id="<?php echo 'post-image' . $post['Post']['id']; ?>">
									<?php
									if (!empty($post['Post']['image_name'])) {
										echo $this->Html->image(
											'post/' . $post['Post']['image_name'], array(
												'alt' => 'Post-picture',
												'class' => 'img-fluid rounded mx-auto d-block my-3')
										);
									}
									?>
								</div>
								<br>
								<small class="text-secondary">
									(<?php echo $this->Time->niceShort($post['Post']['created']); ?>)
								</small>
								<?php
								$sharedPictureName = 'default_profile_picture.png';
								if (isset($post['SharedPost']['User']['ProfilePicture'][0]['image_name'])) {
									$sharedPictureName = $post['SharedPost']['User']['ProfilePicture'][0]['image_name'];
								}
								$sharedPostStatus = (isset($post['SharedPost']['status'])) ? (int)$post['SharedPost']['status'] : null;
								?>
								<div id="<?php echo 'post-rendered' . $post['Post']['id']; ?>">
									<?php if ($sharedPostStatus === 1): ?>
										<div class="bg-light p-3 rounded">
											<div class="bg-light rounded p-3">
												<div class="d-flex flex-row">
													<div class="pl-2 pt-2">
														<div class="profile-picture-post-container">
															<?php
															echo $this->Html->image(
																'profile/' . $sharedPictureName, array(
																	'alt' => 'Profile-picture')
															); ?>
														</div>
													</div>
													<div class="p-2 w-100">
														<div class="row pl-2">
															<div class="col-6">
																<?php
																echo $this->Html->link(
																	$post['SharedPost']['User']['first_name'] . ' ' . $post['SharedPost']['User']['last_name'],
																	array('controller' => 'users', 'action' => 'view', $post['SharedPost']['User']['id']),
																	array('class' => 'font-weight-bold text-primary')
																)
																?>
																<br>
																<small class="text-success">
																	<?php echo nl2br(h($post['SharedPost']['User']['email'])); ?>
																</small>
															</div>
															<div class="col-12 pt-2 my-2">
																	<span class="text-black">
																		<?php echo nl2br(h($post['SharedPost']['content'])); ?>
																	</span>
																<?php
																if (!empty($post['SharedPost']['image_name'])) {
																	echo $this->Html->image(
																		'post/' . $post['SharedPost']['image_name'], array(
																			'alt' => 'Profile-picture',
																			'class' => 'img-fluid rounded mx-auto d-block my-2',
																		)
																	);
																} ?>
																<br>
																<small class="text-secondary">
																	(<?php echo $this->Time->niceShort($post['SharedPost']['created']); ?>
																	)
																</small>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									<?php elseif ($sharedPostStatus === 0): ?>
										<div class="border border-info p-4 rounded">
											<strong>
												Content not available.
											</strong><br>
											This post has been deleted.
										</div>
									<?php endif ?>
								</div>
							</div>
							<?php
							//Define the contents to be shared
							$shareId = $post['Post']['id'];
							$sharePicture = $pictureName;
							$shareFullName = $post['User']['first_name'] . ' ' . $post['User']['last_name'];
							$shareUserId = $post['User']['id'];
							$shareContent = $post['Post']['content'];
							$shareEmail = $post['User']['email'];
							$shareCreatedDate = $post['Post']['created'];
							if (isset($post['SharedPost']['id'])) {
								$shareId = $post['SharedPost']['id'];
								$sharePicture = $sharedPictureName;
								$shareFullName = $post['SharedPost']['User']['first_name'] . ' ' . $post['SharedPost']['User']['last_name'];
								$shareUserId = $post['SharedPost']['User']['id'];
								$shareContent = $post['SharedPost']['content'];
								$shareEmail = $post['SharedPost']['User']['email'];
								$shareCreatedDate = $post['SharedPost']['created'];
							}
							$sharedImage = null;
							if (!empty($post['Post']['image_name'])) {
								$sharedImage = $this->Html->image(
									'post/' . $post['Post']['image_name'], array(
										'alt' => 'Post-picture',
										'class' => 'img-fluid rounded mx-auto d-block my-2')
								);
							}
							if (!empty($post['SharedPost']['image_name'])) {
								$sharedImage = $this->Html->image(
									'post/' . $post['SharedPost']['image_name'], array(
										'alt' => 'Post-picture',
										'class' => 'img-fluid rounded mx-auto d-block my-2')
								);
							}
							?>
							<div class="col-12 pt-2 mb-0 pb-0">
								<small id="likeCount<?php echo $post['Post']['id']; ?>" class="text-info">
									<?php
									if (count($post['Like']) === 1) {
										echo count($post['Like']) . ' like.';
									} elseif (count($post['Like']) > 1) {
										echo count($post['Like']) . ' likes.';
									}
									?>
								</small>
								<div class="d-flex flex-row mb-3">
									<div>
										<?php
										$liked = 'like';
										foreach ($post['Like'] as $like) {
											if ((int)$like['user_id'] === (int)$this->Session->read('Auth.User.id')) {
												$liked = 'unlike';
												break;
											}
										}
										?>
										<button class="btn btn-sm px-3 btn-outline-success ajaxLike"
												data-liked-id="<?php echo $post['Post']['id']; ?>">
											<?php
											echo $liked;
											?>
										</button>
									</div>
									<div>
										<!-- Button trigger modal -->
										<button type="button"
												class="btn btn-sm px-3 btn-outline-success ml-2 share-post-modal-trigger"
												data-target="#sharePostModal" data-post-id="<?php echo $shareId; ?>">
											share
										</button>
									</div>
								</div>
								<!-- Modal For Post Sharing -->
								<div class="d-none" id="sharing-post-content<?php echo $shareId; ?>">
									<?php if ($sharedPostStatus === 1 || $sharedPostStatus === null): ?>
										<div class="bg-light rounded p-3">
											<div class="d-flex flex-row">
												<div class="pl-2 pt-2">
													<div class="profile-picture-post-container">
														<?php
														echo $this->Html->image(
															'profile/' . $sharePicture, array(
																'alt' => 'Profile-picture')
														); ?>
													</div>
												</div>
												<div class="p-2 w-100">
													<div class="row pl-2">
														<div class="col-6">
															<?php
															echo $this->Html->link(
																$shareFullName,
																array('controller' => 'users', 'action' => 'view', $shareUserId),
																array('class' => 'font-weight-bold text-primary')
															)
															?>
															<br>
															<small class="text-success">
																<?php echo nl2br(h($shareEmail)); ?>
															</small>
														</div>
														<div class="col-12 pt-2 my-2">
																	<span class="text-black">
																		<?php echo nl2br(h($shareContent)); ?>
																	</span>
															<?php echo $sharedImage; ?>
															<br>
															<small class="text-secondary">
																(<?php echo $this->Time->niceShort($shareCreatedDate); ?>
																)
															</small>
														</div>
													</div>
												</div>
											</div>
										</div>
									<?php elseif ($sharedPostStatus === 0): ?>
										<div class="border border-info p-4 rounded">
											<strong>
												Content not available.
											</strong><br>
											This post has been deleted.
										</div>
									<?php endif ?>
								</div>
								<?php if ($post['Post']['user_id'] === $this->Session->read('Auth.User.id')): ?>
									<!-- Modal For Post Editing -->
									<div class="modal fade" id="post-update<?php echo $post['Post']['id']; ?>"
										 tabindex="-1"
										 role="dialog"
										 aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
												<?php echo $this->Form->create('Post', array(
													'url' => array(
														'controller' => 'posts',
														'action' => 'edit',
														$post['Post']['id']
													)
												));
												?>
												<div class="modal-header">
													<h5 class="modal-title" id="exampleModalLabel">Edit Post</h5>
													<button type="button" class="close" data-dismiss="modal"
															aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<div class="modal-body">
													<?php echo $this->Form->input('content', array(
														'rows' => 3,
														'class' => 'form-control mb-2',
														'label' => false,
														'placeholder' => 'Say something...',
														'value' => $post['Post']['content']
													)); ?>
													<?php
													if (isset($post['Post']['image_name']) && !empty($post['Post']['image_name'])) {
														echo $this->Html->image(
															'post/' . $post['Post']['image_name'], array(
																'alt' => 'Post-picture',
																'class' => 'img-fluid rounded mx-auto d-block my-2')
														);
													}
													?>
													<?php if (isset($post['SharedPost']['content'])): ?>
														<?php if ($sharedPostStatus === 1): ?>
															<div class="bg-light rounded p-3">
																<div class="d-flex flex-row">
																	<div class="pl-2 pt-2">
																		<div class="profile-picture-post-container">
																			<?php
																			echo $this->Html->image(
																				'profile/' . $sharePicture, array(
																					'alt' => 'Profile-picture')
																			); ?>
																		</div>
																	</div>
																	<div class="p-2 w-100">
																		<div class="row pl-2">
																			<div class="col-6">
																				<?php
																				echo $this->Html->link(
																					$shareFullName,
																					array('controller' => 'users', 'action' => 'view', $shareUserId),
																					array('class' => 'font-weight-bold text-primary')
																				)
																				?>
																				<br>
																				<small class="text-success">
																					<?php echo nl2br(h($shareEmail)) ?>
																				</small>
																			</div>
																			<div class="col-12 pt-2 my-2">
																	<span class="text-black">
																		<?php echo nl2br(h($shareContent)); ?>
																	</span>
																				<?php echo $sharedImage; ?>
																				<br>
																				<small class="text-secondary">
																					(<?php echo $this->Time->niceShort($shareCreatedDate); ?>
																					)
																				</small>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														<?php elseif ($sharedPostStatus === 0): ?>
															<div class="border border-info p-4 rounded">
																<strong>
																	Content not available.
																</strong><br>
																This post has been deleted.
															</div>
														<?php endif ?>
													<?php endif ?>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-secondary"
															data-dismiss="modal">
														Close
													</button>
													<?php echo $this->Form->button(__('save'), array(
														'class' => 'btn btn-success
													')); ?>
												</div>
												<?php echo $this->Form->end(); ?>
											</div>
										</div>
									</div>
								<?php endif ?>
								<div class="border border-light shadow-sm p-3 mb-5 bg-white rounded">
									<?php if ($this->Session->read('Auth.User')) : ?>
										<div class="d-flex flex-row">
											<div class="m-1">
												<div class="profile-picture-comment-container">
													<?php
													echo $this->Html->image(
														'profile/' . $this->Session->read('Auth.User.profile_picture'), array(
															'alt' => 'Profile-picture',
															'class' => 'img-fluid')
													);
													?>
												</div>
											</div>
											<div class="w-100 m-1">
												<?php echo $this->Form->create('Comment', array(
													'url' => array('controller' => 'comments', 'action' => 'comment')
												));
												?>
												<?php echo $this->Form->input('post_id', array(
													'type' => 'hidden',
													'value' => $post['Post']['id']
												));
												?>
												<?php echo $this->Form->input('content', array(
													'rows' => '2',
													'label' => false,
													'class' => 'form-control',
													'placeholder' => 'Say something...'
												));
												?>
												<div class="text-right my-2">
													<?php echo $this->Form->button(__('submit'), array('class' => 'btn btn-outline-success btn-sm')); ?>
												</div>
												<?php echo $this->Form->end(); ?>
											</div>
										</div>
									<?php endif ?>
									<?php if (count($post['Comment']) > 0): ?>
										Comments:
										<div id="clone-comment<?php echo $post['Post']['id']; ?>">
										</div>
										<span data-show-comments-value="1"
											  data-show-comments-target="<?php echo $post['Post']['id']; ?>"
											  class="ajax-comments pointer-on-hover text-info">Load comments...
										</span>
									<?php endif ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endforeach; ?>

<div class="d-flex flex-row">
	<div class="mr-auto">
		<?php echo $this->Paginator->numbers(); ?>
	</div>
	<div class="text-success">
		page <?php echo $this->Paginator->counter(); ?>
	</div>
</div>
<?php
echo $this->element('Modal/delete_post');

echo $this->element('Modal/delete_comment');

echo $this->element('Modal/edit_post');

echo $this->element('Modal/share_post');

echo $this->element('comments/show_comments');
?>
