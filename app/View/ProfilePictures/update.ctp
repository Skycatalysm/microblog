<div class="row justify-content-md-center">
	<div class="col-md-12">
		<?php echo $this->Form->create('ProfilePicture', array('enctype' => 'multipart/form-data')); ?>
		<h5 class="font-weight-bold text-center">Update Profile Picture</h5>
	</div>
	<div class="col-md-12">
		<div class="form-group text-center">
			<?php
			$pictureName = 'default_profile_picture.png';
			if (isset($ProfilePicture['image_name'])) {
				$pictureName = $ProfilePicture['image_name'];
			}
			echo $this->Html->image(
				'profile/' . $pictureName, array(
					'width' => '200px',
					'alt' => 'Profile-picture',
					'class' => 'img-thumbnail Max-width 100% user-view-profile-picture')
			); ?>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<?php
			echo $this->Form->input(
				'image',
				array(
					'class' => 'form-control',
					'placeholder' => 'Enter username',
					'error' => array('attributes' => array('class' => 'text-danger')),
					'type' => 'file'
				)
			);
			echo $this->Form->input('id',
				array(
					'class' => 'form-control',
					'type' => 'hidden'
				)
			);
			?>
		</div>
	</div>
	<div class="col-md-12">
		<div class="text-center">
			<?php echo $this->Form->button(__('Upload'), array('class' => 'btn btn-primary px-5')); ?>
		</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
