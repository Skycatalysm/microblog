<nav class="bg-primary">
	<nav class="navbar navbar-expand-lg navbar-dark container">
		<?php
		echo
		$this->Html->link(
			'MICROBLOG',
			array('controller' => 'posts', 'action' => 'feed'),
			array('class' => 'navbar-brand')
		);
		?>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01"
				aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarColor01">
			<ul class="navbar-nav mr-auto d-flex w-100">
				<?php if ($this->Session->read('Auth.User')) : ?>
					<?php echo $this->Form->create(null, array(
						'class' => 'w-100 my-2 my-md-0',
						'type' => 'get',
						'url' => array('controller' => 'users', 'action' => 'search')
					)); ?>
					<div class="row">
						<div class="col-md">
							<?php echo $this->Form->input('q', array(
								'class' => 'form-control mr-sm-2',
								'label' => false,
								'value' => $this->request->query('q')
							)); ?>
						</div>
						<div class="col-md-3 my-2 my-md-0">
							<?php echo $this->Form->button('Search', array(
								'action' => 'index',
								'class' => 'btn btn-secondary my-2 my-sm-0'
							)); ?>
						</div>
					</div>
					<?php echo $this->Form->end(); ?>
				<?php endif ?>
			</ul>
			<ul class="navbar-nav">
				<?php if ($this->Session->read('Auth.User')) : ?>
					<li class="nav-item">
						<?php
						echo $this->Html->link(
							'HOME',
							array('controller' => 'posts', 'action' => 'feed'),
							array('class' => 'nav-link')
						);
						?>
					</li>
				<?php endif ?>
				<li class="nav-item">
					<?php if ($this->Session->read('Auth.User')) : ?>
						<div class="dropdown">
							<a class="dropdown-toggle nav-link pointer-on-hover text-uppercase" data-toggle="dropdown">
								<?php echo h($this->Session->read('Auth.User.username')); ?>
							</a>
							<div class="dropdown-menu">
								<?php echo
									$this->Html->link(
										'Profile',
										array('controller' => 'users', 'action' => 'view', $this->Session->read('Auth.User.id')),
										array('class' => 'dropdown-item')
									) .
									$this->Html->link(
										'Update Information',
										array('controller' => 'users', 'action' => 'update', $this->Session->read('Auth.User.id')),
										array('class' => 'dropdown-item')
									) .
									$this->Html->link(
										'Update Profile Picture',
										array('controller' => 'profilePictures', 'action' => 'update',
											$this->Session->read('Auth.User.id')),
										array('class' => 'dropdown-item')
									) .
									$this->Html->link(
										'Change Password',
										array('controller' => 'users', 'action' => 'changePassword',
											$this->Session->read('Auth.User.id')),
										array('class' => 'dropdown-item')
									)
								?>
							</div>
						</div>
					<?php else :
						echo $this->Html->link(
							'LOGIN',
							array('controller' => 'users', 'action' => 'login'),
							array('class' => 'nav-link')
						);
					endif
					?>
				</li>
				<li class="nav-item">
					<?php
					if ($this->Session->read('Auth.User')) {
						echo $this->Html->link(
							'LOGOUT',
							array('controller' => 'users', 'action' => 'logout'),
							array('class' => 'nav-link')
						);
					} else {
						echo $this->Html->link(
							'REGISTER',
							array('controller' => 'users', 'action' => 'register'),
							array('class' => 'nav-link')
						);
					}
					?>
				</li>
			</ul>
		</div>
	</nav>
</nav>
