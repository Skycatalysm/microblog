<div id="<?php echo $key; ?>Message"
	 class="mb-3 alert alert-warning alert-dismissible fade show text-center <?php echo !empty($params['class']) ? $params['class'] : 'message'; ?>">
	<?php echo $message; ?>
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</button>
</div>
