<div class="modal fade" id="sharePostModal" tabindex="-1" role="dialog"
	 aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<?php echo $this->Form->create('Post', array(
				'url' => array(
					'controller' => 'posts',
					'action' => 'share'
				)
			));
			?>
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Share Post</h5>
				<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<?php
				$this->Form->unlockField('Post.shared_post_id');
				echo $this->Form->input('shared_post_id', array(
					'type' => 'hidden',
					'id' => 'shareIdTarget'
				)); ?>
				<?php echo $this->Form->input('content', array(
					'rows' => 2,
					'class' => 'form-control mb-2',
					'label' => false,
					'placeholder' => 'Say something...'
				)); ?>
				<div id="share-post-content-target"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">
					Close
				</button>
				<?php echo $this->Form->button(__('share'), array(
					'class' => 'btn btn-success'
				)); ?>
			</div>
			<?php echo $this->Form->end(); ?>
		</div>
	</div>
</div>
