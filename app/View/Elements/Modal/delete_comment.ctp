<div class="modal fade" id="deleteCommentModal" tabindex="-1" role="dialog"
	 aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header text-white bg-danger">
				<h5 class="modal-title" id="exampleModalLabel">
					Are you sure you want to delete this comment?
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<div id="deleteCommentModalBody">
					No comment selected...
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">
					Close
				</button>
				<a id="deleteCommentUrl" href="#">
					<button class="btn btn-danger">
						Delete
					</button>
				</a>
			</div>
		</div>
	</div>
</div>
