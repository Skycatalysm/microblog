<div class="d-none">
	<div id="comment-blueprint" data-comment-auth="<?php echo $this->Session->read('Auth.User.id') ?>" class="m1">
		<div class="d-flex flex-row">
			<div class="pl-2 pt-2">
				<div class="profile-picture-comment-container">
					<?php
					echo $this->Html->image(
						'profile/default_profile_picture.png', array(
							'alt' => 'Profile-picture',
							'class' => 'img-fluid comment-user-picture')
					); ?>
				</div>
			</div>
			<div class="p-2 w-100">
				<div class="row pl-2">
					<div class="col-6">
						<?php
						echo $this->Html->link(
							'default',
							array('controller' => 'users', 'action' => 'view', 0),
							array('class' => 'font-weight-bold text-primary comment-user-full-name')
						)
						?><br>
						<small class="text-success comment-user-email">
							fix22@gmail.com </small>
					</div>
					<div class="col-12 pt-2 my-2">
					<span class="text-black comment-content">
					something
					</span>
						<br>
						<small class="text-secondary comment-created">
							(Friday October 04, 10:36 )
						</small>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
