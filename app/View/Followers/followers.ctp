<div class="d-flex flex-row justify-content-md-center">
	<h5 class="text-center">
		Followers
	</h5>
	<span class="text-dark ml-1">
	(<?php echo $this->Paginator->params()['count']; ?>)
	</span>
</div>
<div class="row justify-content-md-center">
	<?php
	foreach ($followings as $following):
		//Define variables
		$pictureName = 'default_profile_picture.png';
		if (isset($following['User']['ProfilePicture'][0]['image_name'])) {
			$pictureName = $following['User']['ProfilePicture'][0]['image_name'];
		}
		$id = $following['User']['id'];
		$fullName = $following['User']['first_name'] . ' ' . $following['User']['last_name'];
		$email = $following['User']['email'];

		$userID = null;
		if (isset($following['User']['id'])) {
			$userID = $following['User']['id'];
		}
		$followedUserStatus = null;
		if (isset($following['User']['Followers'][0]['user_id']) && ((int)$following['User']['Followers'][0]['user_id']
				=== (int)$this->Session->read('Auth.User.id')) && ((int)$following['User']['Followers'][0]['status'] === 1)) {
			$followedUserStatus = 1;
		}
		?>
		<div class="col-lg-4 col-md-6 col-sm-12 mb-3">
			<div class="border rounded border-dark p-2 h-100">
				<div class="d-flex flex-row bd-highlight mb-3">
					<div class="p-2">
						<div class="profile-picture-post-container">
							<?php echo $this->Html->image(
								'profile/' .$pictureName, array(
									'alt' => 'Profile-picture')
							); ?>
						</div>
					</div>
					<div class="mr-auto p-2">
						<?php
						echo $this->Html->link(
							$this->Text->truncate($fullName, 20,
								array(
									'ellipsis' => '...'
								)),
							array('controller' => 'users', 'action' => 'view', $following['User']['id']),
							array('class' => 'font-weight-bold text-primary')
						)
						?>
						<br>
						<small class="text-success">
							<?php echo $this->Text->truncate(h($email), 25,
								array(
									'ellipsis' => '...'
								)); ?>
						</small>
					</div>
					<div class="row p-2">
						<div class="col-md-6">
							<?php if ($id === $this->Session->read('Auth.User.id')): ?>
							<?php elseif ($followedUserStatus === 1):
								?>
								<?php echo $this->Form->create('follower', array(
								'url' => array('controller' => 'followers', 'action' => 'unfollow')
							));
								?>
								<?php echo $this->Form->input('followed_user_id', array(
								'type' => 'hidden',
								'value' => $id
							));
								?>
								<?php echo $this->Form->button(__('unfollow'), array('class' => 'btn btn-outline-info px-3 btn-sm ml-auto')); ?>
								<?php echo $this->Form->end(); ?>
							<?php else: ?>
								<?php echo $this->Form->create('follower', array(
									'url' => array('controller' => 'followers', 'action' => 'follow')
								));
								?>
								<?php echo $this->Form->input('followed_user_id', array(
									'type' => 'hidden',
									'value' => $id
								));
								?>
								<?php echo $this->Form->button(__('follow'), array('class' => 'btn btn-primary px-3 btn-sm ml-auto')); ?>
								<?php echo $this->Form->end(); ?>
							<?php endif ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endforeach; ?>
	<?php if (empty($followings)) : ?>
		<div class="alert alert-info">
			Currently no follower.
		</div>
	<?php endif ?>
</div>
<div class="d-flex flex-row">
	<div class="mr-auto">
		<?php echo $this->Paginator->numbers(); ?>
	</div>
	<div class="text-success">
		page <?php echo $this->Paginator->counter(); ?>
	</div>
</div>

